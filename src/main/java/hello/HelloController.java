package hello;

import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;
import javax.servlet.http.*;
import java.net.InetAddress;

@RestController
public class HelloController {
    
    @RequestMapping("/")
    public String index(HttpServletRequest request) {
        try {
            InetAddress inetAddress = InetAddress.getLocalHost();

            return "Greetings from Spring Boot: " + inetAddress.getHostAddress() + ", " + inetAddress.getHostName();
        } catch (Exception ex){
            return "Ooops from Spring Boot!";
        }
    }
    
}
